const path              = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTestPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output:{
        filename: 'bundle.js',
        path: path.resolve(__dirname,'dist')
    },
    module:{
        rules:[
            {
                test: /\.scss$/,
                use: ExtractTestPlugin.extract({
                    fallback: "style-loader",
                    loader: ['css-loader','sass-loader'],
                    publicPath: '/dist'
                })

            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Sudoku Solver App',
            template: './src/index.html',
            minify: {
                collapseWhitespace: true
            }
        }),
        new ExtractTestPlugin("app.css")
    ]
}
