const sass = require("./sass/index.scss")

import React from 'react'
import ReactDOM from 'react-dom'
import {SquareSize,Sudoku} from './solver/solver.js'


class Game extends React.Component{
    constructor(props){
        super(props)
        var board = (props.values !== undefined)? props.values.slice(0) : Array(size).fill(0);
        this.solve = this.solve.bind(this)

        this.state = {
            board: board,
            status: 0,
            solution: []
        }
    }

    solve(e){

        let solver = new Sudoku(
            new SquareSize(3,3)
        )

        var solution = this.state.board.slice(0)
        const solved = solver.Solve(solution)


        this.setState({
            status: solved + 1,
            solution: solution
        })
    }

    render(){

        let solution = ""
        if(this.state.status == 2){
            solution = <Board x="3" y="3" identifier="board" highlight="highlight" values={this.state.solution} />
        }

        return <div className="game">
            <div className="initial-board component"> 
            <Board x="3" y="3" identifier="board" highlight="highlight" values={this.state.board} />
            </div>
            <button className="component" onClick={this.solve} disabled={(this.state.status != 0)? 'disabled' : '' }>Solve</button>
            <div className="solution-board component">{solution}</div>
            </div> 
    }
}

class Board extends React.Component{
    constructor(props){
        super(props)
        const size = props.x*props.x*props.y*props.y
        var board = (props.values !== undefined)? props.values.slice(0) : Array(size).fill(0);
        console.log(board);

        if(board.length != size){
            throw new Error("Board lenght and dimensions missmatch")
        }

        this.state = {
            board: board,
            locked: false
        };

        this.handleEnter = this.handleEnter.bind(this)
        this.handleLeave = this.handleLeave.bind(this)
    }

    handleEnter(e){

        this.handleRelatedCells(
            e.target,
            (cell) => {
                cell.classList.add(this.props.highlight) 
            }
        )
    }

    handleLeave(e){

        this.handleRelatedCells(
            e.target,
            (cell) => {
                cell.classList.remove(this.props.highlight)
            }
        )
    }

    handleRelatedCells(cell,cb){

        var rowCells = document.getElementsByClassName(
            "row-" +  cell.dataset.row
        );

        var colCells = document.getElementsByClassName(
            "col-" + cell.dataset.col
        );

        var sqCells = document.getElementsByClassName(
            "sq-" + cell.dataset.sq
        );

        Array.prototype.map.call(rowCells,cb)
        Array.prototype.map.call(colCells,cb)
        Array.prototype.map.call(sqCells,cb)
    }

    render(){
        const size = this.props.x * this.props.y;
        const cells = this.state.board.map((v,i) => {
            const col = Math.floor(i % size)
            const row = Math.floor(i / size)
            const sq  = Math.floor(row/this.props.y)*this.props.y + Math.floor(col/this.props.x)
            return <Cell key={i} value={v} index={i} column={col} row={row} square={sq} enter={this.handleEnter} leave={this.handleLeave} /> 
        })

        return <div className={this.props.identifier}>{cells}</div>
    }
}

class Cell extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        var classes = [
            "cell",
            "cell-" + this.props.index,
            "row-"  + this.props.row,
            "col-" + this.props.column,
            "sq-"  + this.props.square
        ];
        return (
            <div className={classes.join(' ')} 
                 data-row={this.props.row} 
                 data-col={this.props.column}
                 data-sq={this.props.square}
                 onMouseEnter={this.props.enter}
                 onMouseLeave={this.props.leave}
            >
                {this.props.value}
            </div>
        );
    }
}

var values = [
    "","","",   "",5,1,     9,7,"",
    3,"",7,     9,"","",    "","",2,
    "","","",   3,2,"",     1,"",4,

    "","","",   1,9,8,       7,"","",
    "",3,"",   "",7,"",     "",2,"",
    "","",9,    2,3,6,      "","","",

    9,"",5,     "",4,2,     "","","",
    4,"","",    "","",9,    2,"",8,
    "",8,2,     5,6,"",     "","","",
]

ReactDOM.render(
    <Game values={values} />,
    //<Board x="3" y="3" identifier="board" highlight="highlight" values={values} />,
   //<Cell index="34" value="24" />,
    document.getElementById('root')
)

/*
console.log(values)
let solver = new Sudoku(
    new SquareSize(3,3)
)

const result = solver.Solve(values)
console.log(result)
console.log(values)
*/